## Setup

- Crear in virtual environment con _python3_ e instalar la lista de requirements
- Para levantar el server de Flask basta ejecutar `python wsgi.py`, el servidor corre sobre `localhost:5000`

## Guías básicas

- Seguimos PEP8 para el estilo del código
- Los objetos para uso global deben crearse en `extensions.py`
- Los objetos globales se importan desde `extensions.py` y se inicializan en `factory.py`
- Donde sea necesario utilizar el objeto **app** que representa la aplicación, usamos blueprints, usar como referencia `views.py`
- La biblioteca **FLASK CORS** se utiliza para decorar los endpoints que serán consumidos por el frontend en VueJS

## Workflow

Para agregar un nuevo feature a la aplicación crear un ticket, describiendo brevemente el feature y su comportamiento. Crear una rama utilizando como convenio para el nombre **user/ticket_name**

Esto facilita que no haya colaboradores trabajando en el mismo feature a la vez y que en todo momento se sepa quién agregó un feature a la aplicación, para poder hacerle preguntas :)

Happy Coding!
