"""Module containing all views"""

from flask import Blueprint
from flask_cors import cross_origin


app_views = Blueprint('app_views', __name__)

@app_views.route('/', methods=['GET'])
def home():
    """Hello World page"""
    return "HELLO WORLD"

@app_views.route('/vue', methods=['GET'])
@cross_origin()
def vue_demo():
    """Vue demo page"""
    return "VUE NEEDS CORS FLASK-CORS"
