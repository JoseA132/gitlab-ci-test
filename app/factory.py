"""Module containing all factory methods"""

from flask import Flask
from .extensions import cors
from .views import app_views


def create_app():
    """API app factory"""
    _app = Flask(__name__)
    cors.init_app(_app)
    _app.register_blueprint(app_views)
    return _app


app = create_app()
